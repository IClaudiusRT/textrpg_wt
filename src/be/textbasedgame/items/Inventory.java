package be.textbasedgame.items;

import be.textbasedgame.character.PlayableCharacter;
import be.textbasedgame.combat.actions.attacks.WeaponAttack;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Inventory implements Serializable {

    //Adding and removing items from inventory

    List<Item> inventory = new ArrayList<>();
    int count;
    int size;
    Map<ItemVariables, Item> equiped = new HashMap<>();

    public Inventory(int size) {
        this.size = size;
    }

    public String[] getListNames() {
        String[] finalListNames = new String[inventory.size()];
        List<String> listStringNames = new ArrayList<>();
        for (Item el : inventory) {
            listStringNames.add(el.getName().toLowerCase());
        }
        listStringNames.toArray(finalListNames);
        return finalListNames;
    }

    public List<Item> listInventory(Inventory inventory) {
        return inventory.inventory;
    }

    public void removeItem(Item item) {
        if (item.getEnumType().isDroppable()) {
            inventory.remove(item);
        } else {
            System.out.println("You can't drop a quest item!");
        }
    }

    public void addItem(Item item) {
        if (!isIventoryFull()) {
            inventory.add(item);
        } else {
            System.out.println("Inventory is full, remove an item first.");
        }
    }

    public boolean isIventoryFull() {
        if (inventory.size() >= size) {
            return true;
        } else {
            return false;
        }
    }

    public Item giveItemOnIndex(int index) {
        Item[] tempItems = inventory.stream().toArray(Item[]::new);
        return tempItems[index];
    }

    public void printItems() {
        System.out.println("\n--------------Inventory------------");
        for (Item el : inventory) {
            if (el.equals(ItemVariables.WEAPON.getEquippedItem())) {
                System.out.println(++count + " " + el.getName() + " \t\tEquipped Weapon");
            } else if (el.equals(ItemVariables.ARMOR.getEquippedItem())) {
                System.out.println(++count + " " + el.getName() + " \t\tEquipped Armor");
            } else {
                System.out.println(++count + " " + el.getName());
            }
        }
        count = 0;
    }

    //Using items already inside inventory

    public void equipItem(Item item) {
        if (item.getEnumType().isEquippable() && item.getEnumType().isEmpty()) {
            item.getEnumType().setEquippedItem(item);
            equiped.put(item.getEnumType(), item);
        } else {
            System.out.println("You already have an equipped item, unequip that first.");
        }
    }

    public void removeEquippedItem(Item item) {
        item.getEnumType().removeEquippedItem(item);
        equiped.remove(item.getEnumType());
    }

    public void consumeItem(Item item/*, PlayableCharacter pc*/) {
        if (item.getEnumType().isConsumable()) {
//            creature.getHp()+=((HealthItem)item).getGiveHP();         //HealthItem has to do something TODO
//            System.out.println(pc.getName() + " has consumed " + item.getName());
            System.out.println("You have consumed the " + item.getName());
            removeItem(item);
        } else {
            System.out.println("This item is not consumable!");
        }

    }

    public void printEquippedItems() {
        System.out.println("You currently have these items equipped: ");
        System.out.println("Weapon:\t" + ItemVariables.WEAPON.getEquippedItem().getDescription());
        System.out.println("Armor:\t" + ItemVariables.ARMOR.getEquippedItem().getDescription());
    }

    public void getDescriptionItem(Item item) {
        System.out.println(item.getDescription());
    }

    public Weapon getEquipedWeapon() {
        return (Weapon) equiped.get(ItemVariables.WEAPON);
    }

    public Map<String, Item> makeConsumableMap() {
        Map<String, Item> consumableMap = new HashMap<>();
        for (Item i : inventory) {
            if (i.getEnumType().isConsumable()) {
                consumableMap.put(i.getName(), i);
            }
        }
        return consumableMap;
    }
}





