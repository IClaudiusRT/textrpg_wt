package be.textbasedgame.items;

public class BasicItem extends Item{

    public BasicItem(String name, String description, ItemVariables itemVariables) {         //for items without stats, like a letter
        super(name, description, itemVariables);
    }
}
