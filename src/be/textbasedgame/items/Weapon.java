package be.textbasedgame.items;

import be.textbasedgame.utility.Die;

public class Weapon extends Item {

    private Die[] dice;
    private int damageModifier;
    boolean isEquippableWeapon;
    private ItemVariables weapon;

    public Weapon(String name, String description, ItemVariables itemVariables, Die[] baseDamage, int damageModifier) {     //for items that increase damage, like a dagger
        super(name, description, itemVariables);
        this.damageModifier = damageModifier;
        this.dice = baseDamage;
    }

    public int getDamageModifier() {
        return damageModifier;
    }

    public boolean isEquippableWeapon() {
        return true;
    }

    public String determineEnumType() {
        return ItemVariables.WEAPON.name();
    }

    public int getBaseDamage() {
        int baseDamage = 0;
        for (Die d : dice) {
            baseDamage += d.roll();
        }
        return baseDamage;
    }
}
