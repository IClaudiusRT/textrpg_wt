package be.textbasedgame.items;

import be.textbasedgame.character.*;
import be.textbasedgame.character.race.Human;
import be.textbasedgame.character.fantasyclass.Warrior;
import be.textbasedgame.utility.Die;

public class TestItem {
    public static void main(String[] args) {

        //making new Inventory and Inventory Menu to test

        PlayableCharacter mc = new PlayableCharacter("John", Gender.MALE, new Human(),new Warrior(), new Inventory(20));

        //Adding some items to test with
        Weapon weaponRogue = new Weapon("Dagger","A small obsidian dagger", ItemVariables.WEAPON, new Die[] {new Die(4)}, 5);
        mc.getInventory().addItem(weaponRogue);
        Weapon weaponWarrior = new Weapon("Sword", "A rusted steel sword", ItemVariables.WEAPON, new Die[] {new Die(6), new Die(6)}, 5);
        mc.getInventory().addItem(weaponWarrior);
        Armor leatherArmor = new Armor("Leather Armor", "Leather armor that will protect somewhat", ItemVariables.ARMOR,5);
        mc.getInventory().addItem(leatherArmor);
        Armor leatherGloves = new Armor("Leather Gloves", "These are some leather gloves", ItemVariables.GLOVES,3);
        HealthItem hpPot = new HealthItem("Health Potion", "Drink this potion during combat to regain some HP", ItemVariables.POTION,30);
        mc.getInventory().addItem(hpPot);
        BasicItem letter = new BasicItem("A handwritten letter", "blbablabla", ItemVariables.QUESTITEM);
        mc.getInventory().addItem(letter);

        //Testing the menu
        mc.getInventoryMenu();



    }
}
