package be.textbasedgame.items;

public class Armor extends Item {

    private int armorModifier;
    private ItemVariables armor;

    public Armor(String name, String description, ItemVariables itemVariables, int armorModifier) {
        super(name, description, itemVariables);
        this.armorModifier = armorModifier;
    }

    public int getArmorModifier() {
        return armorModifier;
    }
}
