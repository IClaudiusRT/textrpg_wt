package be.textbasedgame.items;

public enum ItemVariables {

    WEAPON(true, true, false, false),
    ARMOR(true, true, false, false),
    QUESTITEM(false, false, true, true),
    POTION(false, true, true, true),
    GLOVES(true, true, false, false);

    private boolean equippable;
    private boolean droppable;
    private boolean consumable;
    private boolean stackable;
    private Item equippedItem;

    ItemVariables(boolean equippable, boolean droppable, boolean consumable, boolean stackable) {
        this.equippable = equippable;
        this.droppable = droppable;
        this.consumable = consumable;
        this.stackable = stackable;

    }

    public boolean isEquippable() {
        return equippable;
    }

    public boolean isDroppable() {
        return droppable;
    }

    public boolean isConsumable() {
        return consumable;
    }

    public boolean isStackable() {
        return stackable;
    }

    public void setEquippedItem(Item equippedItem) {
        this.equippedItem = equippedItem;
    }

    public void removeEquippedItem(Item equippedItem) {
        this.equippedItem = null;
    }

    public Item getEquippedItem() {
        return equippedItem;
    }

    public boolean isEmpty() {
        if (getEquippedItem() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isWeaponDroppable(Weapon weapon) {
        return WEAPON.droppable;
    }


    public boolean isEquipped(Item item) {
        if (item.equals(getEquippedItem())){
            return true;
        } else {
            return false;
        }
    }
}
