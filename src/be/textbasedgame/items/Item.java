package be.textbasedgame.items;

import java.io.Serializable;

public abstract class Item implements Serializable {

    private String name;
    private String description;
    private ItemVariables itemVariables;


    public Item(String name, String description, ItemVariables itemVariables){
        this.name = name;
        this.description = description;
        this.itemVariables = itemVariables;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ItemVariables getEnumType(){
        return itemVariables;
    }


}



