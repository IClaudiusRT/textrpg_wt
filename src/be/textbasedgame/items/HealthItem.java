package be.textbasedgame.items;

public class HealthItem extends Item {

    private int giveHP;

    public HealthItem(String name, String description, ItemVariables itemVariables, int giveHP) {
        super(name, description, itemVariables);
        this.giveHP = giveHP;
    }

    public int getGiveHP() {
        return giveHP;
    }
}
