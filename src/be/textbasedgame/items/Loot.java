package be.textbasedgame.items;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.Team;
import be.textbasedgame.items.Item;
import be.textbasedgame.map.Encounterable;

import java.util.ArrayList;

public class Loot implements Encounterable {
    private String name;
    private ArrayList<Item> lootTable;

    public Loot(String name, ArrayList<Item> items) {
        this.name = name;
        this.lootTable = items;
    }

    public String getEncounterDescription() {
        return "a " + name;
    }

    @Override
    public void interact(Team initiator) {

    }

    @Override
    public String getPossibleAction() {
        return "open " + name;
    }

    @Override
    public boolean finished() {
        return lootTable.size() == 0;
    }
}
