package be.textbasedgame.items;

import be.textbasedgame.utility.Die;

public enum AllItems {

    //Weapons
    STEELSWORD(new Weapon("Steel Sword", "A steel sword that has seen a fair bit of combat"
            , ItemVariables.WEAPON , new Die[] {new Die(6)},5)),
    DAGGER(new Weapon("Dagger", "A small, well-maintained knife", ItemVariables.WEAPON, new Die[] {new Die(4)},0)),
    OBSIDIANDAGGER(new Weapon("Obsidian Dagger", "A small obsidian knife, sharp as glass", ItemVariables.WEAPON, new Die[] {new Die(8)},0)),
    WAND(new Weapon("Wand", "An ebony wand", ItemVariables.WEAPON, new Die[] {new Die(6)}, 0)),
    BOW(new Weapon("Bow", "A mahogany string bow", ItemVariables.WEAPON, new Die[] {new Die(6)}, 0)),

    //Armor
    LEATHERARMOR(new Armor("Leather Armor", "Leather armor that will protect somewhat", ItemVariables.ARMOR, 5)),
    CHAINMALE(new Armor("Chain Mail", "Chain male armor that provides a lot of protection but limits movement"
            , ItemVariables.ARMOR, 10)),
    ROBES(new Armor("Robes Mage","Mage Robes, not much protection against physical attacks but it sure had other valuable uses."
            ,ItemVariables.ARMOR,4)),

    //Health Items
    HEALTHPOTION(new HealthItem("Health Potion", "Drink this potion during combat to regain some HP", ItemVariables.POTION,30)),
    LARGEHEALTHPOTION(new HealthItem("Large Health Potion", "Drink this potion during combat to regain some HP", ItemVariables.POTION,50)),


    //Quest Items
    LETTER(new BasicItem("A handwritten letter", "blbablabla", ItemVariables.QUESTITEM));

    private Item item;

    AllItems(Item itemValue) {
        this.item=itemValue;
    }

    public Item getItem(){
        return item;
    }
}
