package be.textbasedgame.combat;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.actions.Targetability;
import be.textbasedgame.utility.ChoiceMenu;
import be.textbasedgame.utility.ConsoleInput;
import be.textbasedgame.utility.Printer;

import java.util.*;

public class Combat {
    private Team heroes;
    private Team enemies;
    private Creature[] order;


    public Combat(Team protagonists, Team antagonists) {
        this.heroes = protagonists;
        this.enemies = antagonists;
        start();
    }

    public void start() {
        initiate();
        while (!combatFinished()) {
            combatRound();
        }

    }

    private void combatRound() {
        for (Creature c : order) {
            Printer.getInstance().creatureCondition(c);
            if (!combatFinished() || !c.incapacitated()) {
                Printer.getInstance().turnMessage(c);
                boolean actionTaken = false;
                while (!actionTaken) {
                    Action a = c.useAction();
                    a.setCaster(c);
                    Team targetOptions = getTarget(c, a.getTargetability());
                    a.setTarget(new Team(c.target(targetOptions)));
                    actionTaken = a.resolve();
                }
                ConsoleInput.getInstance().waitForInput();
            }
            heroes.condition("team 1");
            enemies.condition("team 2");

            try {
                Thread.sleep(300L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private Team getTarget(Creature c, Targetability targetability) {
        Map<String, Creature> possibleTargets = new HashMap<>();
        switch (targetability) {
            case ENEMIES:
                if (heroes.contains(c)) {
                    possibleTargets = enemies.getTeam();
                } else {
                    possibleTargets = heroes.getTeam();
                }
                break;
            case FRIENDS:
                if (heroes.contains(c)) {
                    possibleTargets = heroes.getTeam();
                } else {
                    possibleTargets = enemies.getTeam();
                }
                break;
            case EVERYONE:
                possibleTargets = heroes.getTeam();
                possibleTargets.putAll(enemies.getTeam());
                break;
            case SELF:
                possibleTargets.put(c.getName(), c);
        }
        return new Team(possibleTargets);
    }

    private void initiate() {
        Creature[] all = new Creature[heroes.getPartySize() + enemies.getPartySize()];
        HashMap<Creature, Integer> initiativeRolls = new HashMap<>();
        int index = 0;
        for (Creature c : heroes.getTeamAsArray()) {
            all[index++] = c;
        }
        for (Creature c : enemies.getTeamAsArray()) {
            all[index++] = c;
        }
        for (Creature c : all) {
            int initiative = c.rollForInitiative();
            Printer.getInstance().initiativeRoll(c, initiative);
            initiativeRolls.put(c, c.rollForInitiative());
        }

        //doesn't seem to work properly
        order = Arrays.stream(all)
                .sorted(Comparator.comparingInt(initiativeRolls::get).reversed())
                .toArray(Creature[]::new);

        Printer.getInstance().listOrder(order);
    }

    private boolean combatFinished() {
        return heroes.getTeamHp() == 0 || enemies.getTeamHp() == 0;
    }
}
