package be.textbasedgame.combat;

import be.textbasedgame.character.Creature;
import be.textbasedgame.map.Encounterable;
import be.textbasedgame.utility.Printer;

import java.io.Serializable;
import java.util.*;

public class Team implements Encounterable, Serializable {
    private Map<String, Creature> team = new HashMap<>();
    private int teamHP;

    public Team(Creature... team) {
        for (Creature c : team) {
            this.team.put(c.getName(),c);
        }
        teamHP = getTeamHp();
    }

    public Team(Map<String, Creature> team) {
        this.team = team;
    }

    public Creature[] getTeamAsArray() {
        return team.values().toArray(new Creature[0]);
    }

    public Map<String, Creature> getTeam() {
        return team;
    }

    public int getPartySize() {
        return team.size();
    }

    public int getTeamHp() {
        teamHP = 0;
        for (Creature c : team.values()) {
            teamHP += c.getHp();
        }
        return teamHP;
    }

    public boolean contains(Creature c) {
        return team.containsValue(c);
    }

    public Creature random() {
        int random = new Random().nextInt(team.size());
        return team.values().toArray(Creature[]::new)[random];
    }

    public void condition(String name) {
        getTeamHp();
        Printer.getInstance().teamCondition(this , name);
    }

    public Creature get(String name) {
        return team.get(name);
    }

    @Override
    public String getEncounterDescription() {
        return null;
    }

    @Override
    public void interact(Team initiator) {
        Combat combat = new Combat(initiator, this);
    }

    @Override
    public String getPossibleAction() {
        return "attack";
    }

    @Override
    public boolean finished() {
        return getTeamHp() == 0;
    }

    public String[] getTeamNames() {
        ArrayList<String> names = new ArrayList<>();
        for (Creature c : team.values()) {
            names.add(c.getName());
        }
        return names.toArray(new String[0]);
    }
}
