package be.textbasedgame.combat.actions.skills;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.actions.Targetability;
import be.textbasedgame.utility.Printer;

public class ShieldBash extends Skill{
    private int skillPointCost = 10;

    @Override
    public boolean resolve() {
        boolean succes = false;
        if (caster.getSp() >= skillPointCost) {
            caster.useSp(skillPointCost);
            for (Creature c : super.target.getTeamAsArray()) {
                int damage = 6 + caster.getAttributes().getStrength()/2;
                c.takeDamage(damage);
                Printer.getInstance().damageMessage(caster.getName(), damage, "shield bash", c.getName());
                succes = true;
            }
        } else {
            Printer.getInstance().actionNotPossible();
            succes = false;
        }
        return succes;
    }

    @Override
    public Targetability getTargetability() {
        return Targetability.ENEMIES;
    }

}
