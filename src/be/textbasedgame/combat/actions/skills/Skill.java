package be.textbasedgame.combat.actions.skills;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.Team;
import be.textbasedgame.combat.actions.Action;

public abstract class Skill implements Action {
    protected Creature caster;
    protected Team target;
    protected int skillPointCost;

    @Override
    public void setTarget(Team target) {
        this.target = target;
    }

    @Override
    public void setCaster(Creature caster) {
        this.caster = caster;
    }

    public int getSkillPointCost() {
        return skillPointCost;
    }
}
