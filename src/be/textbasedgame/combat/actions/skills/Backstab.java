package be.textbasedgame.combat.actions.skills;

import be.textbasedgame.combat.actions.Targetability;

public class Backstab extends Skill {
    @Override
    public boolean resolve() {
        return false;
    }

    @Override
    public Targetability getTargetability() {
        return Targetability.ENEMIES;
    }
}
