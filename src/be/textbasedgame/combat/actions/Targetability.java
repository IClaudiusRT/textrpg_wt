package be.textbasedgame.combat.actions;

public enum Targetability {
    ENEMIES,
    FRIENDS,
    SELF,
    EVERYONE
}
