package be.textbasedgame.combat.actions.attacks;

import be.textbasedgame.character.Creature;
import be.textbasedgame.character.Monster;
import be.textbasedgame.combat.actions.Targetability;
import be.textbasedgame.utility.Die;
import be.textbasedgame.utility.Printer;

public class MonsterAttack extends Attack {

    private int damageModifier;
    private Die[] dice;

    public MonsterAttack(Die[] damageDice, int damageModifier) {
        super();
        this.dice = damageDice;
        this.damageModifier = damageModifier;
    }

    @Override
    public boolean resolve() {
        int damage = baseDamage() + damageModifier();
        for (Creature c : target.getTeamAsArray()) {
            c.takeDamage(damage);
            Printer.getInstance().damageMessage(attacker.getName(), damage, "bite", c.getName());
        }
        return true;
    }

    @Override
    protected int baseDamage() {
        int damage = 0;
        for (Die d : dice) {
            damage += d.roll();
        }
        return damage;
    }

    @Override
    protected int damageModifier() {
        return damageModifier;
    }

    @Override
    public Targetability getTargetability() {
        return Targetability.ENEMIES;
    }
}
