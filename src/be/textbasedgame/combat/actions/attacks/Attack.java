package be.textbasedgame.combat.actions.attacks;

import be.textbasedgame.character.Creature;
import be.textbasedgame.character.HumanoidCharacter;
import be.textbasedgame.combat.Team;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.actions.Targetability;
import be.textbasedgame.items.ItemVariables;
import be.textbasedgame.items.Weapon;
import be.textbasedgame.utility.Die;
import be.textbasedgame.utility.Printer;

public abstract class Attack implements Action {

    protected Team target;
    protected Creature attacker;

    protected abstract int baseDamage();

    protected abstract int damageModifier();

    @Override
    public Targetability getTargetability() {
        return Targetability.ENEMIES;
    }

    @Override
    public void setTarget(Team target) {
        this.target = target;
    }

    @Override
    public void setCaster(Creature caster) {
        this.attacker = caster;
    }
}
