package be.textbasedgame.combat.actions.attacks;

import be.textbasedgame.character.Creature;
import be.textbasedgame.items.ItemVariables;
import be.textbasedgame.items.Weapon;
import be.textbasedgame.utility.Die;
import be.textbasedgame.utility.Printer;

public class WeaponAttack extends Attack {

    private Weapon weapon = new Weapon("fists", "ye ol' fisticuffs", ItemVariables.WEAPON, new Die[] {new Die(4)}, 0); //TODO get eqiupped weapon from attacker

    @Override
    protected int baseDamage() {
        return weapon.getBaseDamage();
    }

    @Override
    protected int damageModifier() {
        return attacker.getAttributes().getStrength() / 2;
    }

    @Override
    public boolean resolve() {
        int damage = baseDamage() + damageModifier();
        for (Creature c : target.getTeamAsArray()) {
            c.takeDamage(damage);
            Printer.getInstance().damageMessage(attacker.getName(), damage, weapon.getName(), c.getName());
        }
        return true;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;

    }


}
