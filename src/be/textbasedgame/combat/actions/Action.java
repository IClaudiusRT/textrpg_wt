package be.textbasedgame.combat.actions;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.Team;

public interface Action {
    boolean resolve();

    Targetability getTargetability();

    void setTarget(Team Target);

    void setCaster(Creature caster);
}
