package be.textbasedgame.combat.actions.itemusage;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.Team;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.items.Item;

public abstract class ItemUse implements Action {
    protected Creature caster;
    protected Team target;
    protected int skillPointCost;
    protected Item item;

    @Override
    public void setTarget(Team target) {
        this.target = target;
    }

    @Override
    public void setCaster(Creature caster) {
        this.caster = caster;
    }

    public int getSkillPointCost() {
        return skillPointCost;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
