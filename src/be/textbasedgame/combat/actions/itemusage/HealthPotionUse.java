package be.textbasedgame.combat.actions.itemusage;

import be.textbasedgame.character.Creature;
import be.textbasedgame.character.HumanoidCharacter;
import be.textbasedgame.combat.Team;
import be.textbasedgame.combat.actions.Targetability;
import be.textbasedgame.items.HealthItem;

public class HealthPotionUse extends ItemUse {

    @Override
    public boolean resolve() {
        ((HumanoidCharacter) caster).getInventory().consumeItem(item);
        caster.heal(((HealthItem) item).getGiveHP());
        return true;
    }

    @Override
    public Targetability getTargetability() {
        return Targetability.FRIENDS;
    }
}
