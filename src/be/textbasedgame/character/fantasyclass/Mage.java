package be.textbasedgame.character.fantasyclass;

import be.textbasedgame.character.ActionUse;
import be.textbasedgame.character.Attributes;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.actions.skills.ThunderBolt;
import be.textbasedgame.items.AllItems;
import be.textbasedgame.items.Inventory;
import be.textbasedgame.utility.ChoiceMenu;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Mage extends FantasyClass implements Serializable {
    private Attributes attributes = new Attributes(0, 2, 3, 0, 0, 0);
    private transient Map<String, ActionUse> skills = makeSkillMap();

    public Mage() {
        super();
    }


    @Override
    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String getDescription() {
        return "You are a mage. + Extra info";
    }

    @Override
    public int getSpecialPoint(Attributes atts) {
        return atts.getWisdom() * 5;

    }

    @Override
    public Action useSkill() {
        ChoiceMenu cm = new ChoiceMenu(makeSkillMap().keySet().toArray(new String[0]));
        return makeSkillMap().get(cm.getChoice()).run();
    }

    @Override
    public Map<String, ActionUse> makeSkillMap() {
        Map<String, ActionUse> temp = new HashMap<>();

        temp.put("thunder bolt", this::thunderBolt);
        return temp;
    }

    @Override
    public Inventory initializeInventory() {
        Inventory initialInventory = new Inventory(20);
        initialInventory.addItem(AllItems.WAND.getItem());
        initialInventory.addItem(AllItems.ROBES.getItem());
        initialInventory.addItem(AllItems.HEALTHPOTION.getItem());
        return initialInventory;
    }

    private Action thunderBolt() {
        return new ThunderBolt();
    }

    @Override
    public String toString() {
        return "Mage";
    }


}
