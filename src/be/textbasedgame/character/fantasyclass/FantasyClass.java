package be.textbasedgame.character.fantasyclass;

import be.textbasedgame.character.ActionUse;
import be.textbasedgame.character.Attributes;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.items.Inventory;

import java.util.Map;

public abstract class FantasyClass {
    private transient Map<String, ActionUse> skills;

    public FantasyClass() {
        skills = makeSkillMap();
    }

    public abstract Attributes getAttributes();

    public abstract String getDescription();

    public abstract int getSpecialPoint(Attributes atts);

    public abstract Action useSkill();

    public abstract Map<String, ActionUse> makeSkillMap();

    public abstract Inventory initializeInventory();
}
