package be.textbasedgame.character.fantasyclass;

import be.textbasedgame.character.ActionUse;
import be.textbasedgame.character.Attributes;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.actions.skills.PiercingShot;
import be.textbasedgame.items.AllItems;
import be.textbasedgame.items.Inventory;
import be.textbasedgame.utility.ChoiceMenu;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Ranger extends FantasyClass implements Serializable {

    private Attributes attributes = new Attributes(0, 0, 1, 3, 0, 0);
    private transient Map<String, ActionUse> skills = makeSkillMap();

    public Ranger() {
        super();
    }

    @Override
    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String getDescription() {
        return "You are a ranger.";
    }

    @Override
    public int getSpecialPoint(Attributes atts) {
        return atts.getDexterity() * 5;
    }

    @Override
    public Action useSkill() {
        ChoiceMenu cm = new ChoiceMenu(makeSkillMap().keySet().toArray(new String[0]));
        return makeSkillMap().get(cm.getChoice()).run();
    }

    @Override
    public Map<String, ActionUse> makeSkillMap() {
        Map<String, ActionUse> temp = new HashMap<>();

        temp.put("piercing shot", this::piercingShot);
        return temp;
    }

    @Override
    public Inventory initializeInventory() {
        Inventory initialInventory = new Inventory(20);
        initialInventory.addItem(AllItems.BOW.getItem());
        initialInventory.addItem(AllItems.LEATHERARMOR.getItem());
        initialInventory.addItem(AllItems.HEALTHPOTION.getItem());
        return initialInventory;
    }

    private Action piercingShot() {
        return new PiercingShot();
    }

    @Override
    public String toString() {
        return "Ranger";
    }

}
