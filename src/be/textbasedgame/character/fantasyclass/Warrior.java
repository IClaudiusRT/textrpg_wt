package be.textbasedgame.character.fantasyclass;

import be.textbasedgame.character.ActionUse;
import be.textbasedgame.character.Attributes;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.actions.skills.ShieldBash;
import be.textbasedgame.items.*;
import be.textbasedgame.utility.ChoiceMenu;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Warrior extends FantasyClass implements Serializable {

    private Attributes attributes = new Attributes(3, 0, 0, 0, 2, 0);
    private transient Map<String, ActionUse> skills = makeSkillMap();

    public Warrior() {
        super();
    }

    @Override
    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String getDescription() {
        return "You are a warrior. + Extra info";
    }

    @Override
    public int getSpecialPoint(Attributes atts) {
        return atts.getDexterity() * 5;
    }

    @Override
    public Action useSkill() {
        ChoiceMenu cm = new ChoiceMenu(makeSkillMap().keySet().toArray(new String[0]));
        return makeSkillMap().get(cm.getChoice()).run();
    }

    @Override
    public Map<String, ActionUse> makeSkillMap() {
        Map<String, ActionUse> temp = new HashMap<>();

        temp.put("shield bash", this::shieldBash);
        return temp;
    }


    public Action shieldBash() {
        return new ShieldBash();
    }

    @Override
    public String toString() {
        return "Warrior";
    }

    @Override
    public Inventory initializeInventory() {
        Inventory initialInventory = new Inventory(20);             //need to set size somewhere TODO
        initialInventory.addItem(AllItems.STEELSWORD.getItem());
        initialInventory.equipItem(AllItems.STEELSWORD.getItem());
        initialInventory.addItem(AllItems.CHAINMALE.getItem());
        initialInventory.addItem(AllItems.HEALTHPOTION.getItem());
        return initialInventory;
    }
}
