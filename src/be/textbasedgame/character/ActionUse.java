package be.textbasedgame.character;

import be.textbasedgame.combat.actions.Action;

public interface ActionUse {

    Action run();
}
