package be.textbasedgame.character;

import java.io.Serializable;

public class Attributes implements Serializable {
    private int strength = 10;
    private int intelligence = 10;
    private int wisdom = 10;
    private int dexterity = 10;
    private int constitution = 10;
    private int charisma = 10;

    public Attributes() {

    }

    public Attributes(int strength, int intelligence, int wisdom, int dexterity, int constitution, int charisma) {
        this.strength = strength;
        this.intelligence = intelligence;
        this.wisdom = wisdom;
        this.dexterity = dexterity;
        this.constitution = constitution;
        this.charisma = charisma;
    }

    public void addAttributes(Attributes a) {
        this.strength += a.getStrength();
        this.intelligence += a.getIntelligence();
        this.wisdom += a.getWisdom();
        this.dexterity += a.getDexterity();
        this.constitution += a.getConstitution();
        this.charisma += a.getCharisma();
    }

    public int getStrength() {
        return strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getWisdom() {
        return wisdom;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getConstitution() {
        return constitution;
    }

    public int getCharisma() {
        return charisma;
    }

    public String printAttributes() {
        return "Strenght: " + getStrength() + " | Intelligence: " + getIntelligence() + " | Wisdom " + getWisdom()
                + " | Dexterity: " + getDexterity() + " | Constitution: " + getConstitution() + " | Charisma: " + getCharisma();
    }
}
