package be.textbasedgame.character;

import be.textbasedgame.character.fantasyclass.FantasyClass;
import be.textbasedgame.character.race.Race;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.items.Inventory;
import be.textbasedgame.combat.Team;
import be.textbasedgame.utility.ChoiceMenu;

import java.io.Serializable;

public class PlayableCharacter extends HumanoidCharacter implements Serializable {

    public PlayableCharacter(String name, Gender gender, Race race, FantasyClass fc, Inventory inventory) {
        super(name, gender, race, fc, inventory);
    }

    @Override
    public Action useAction() {
        ChoiceMenu cm = new ChoiceMenu(super.makeActionMap().keySet().toArray(new String[0]));
        return super.makeActionMap().get(cm.getChoice()).run();
    }

    @Override
    public Creature target(Team targetOptions) {

        ChoiceMenu cm = new ChoiceMenu(targetOptions.getTeamNames());
        return targetOptions.get(cm.getChoice());
    }
}
