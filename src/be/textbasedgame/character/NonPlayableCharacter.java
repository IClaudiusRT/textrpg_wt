package be.textbasedgame.character;

import be.textbasedgame.character.fantasyclass.FantasyClass;
import be.textbasedgame.character.race.Race;
import be.textbasedgame.items.Inventory;

public class NonPlayableCharacter extends HumanoidCharacter {

    public NonPlayableCharacter(String name, Gender gender, Race race, FantasyClass fc, Inventory inventory) {
        super(name, gender, race, fc, inventory);
    }

}
