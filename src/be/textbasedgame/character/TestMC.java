package be.textbasedgame.character;

import be.textbasedgame.character.race.Dwarf;
import be.textbasedgame.character.race.Human;
import be.textbasedgame.character.fantasyclass.Rogue;
import be.textbasedgame.character.fantasyclass.Warrior;
import be.textbasedgame.items.Inventory;

public class TestMC {
    public static void main(String[] args) {
//        PlayableCharacter mc = new PlayableCharacter("John", Gender.MALE, new Human(),new Mage());
        PlayableCharacter mc2 = new PlayableCharacter("Janet", Gender.FEMALE, new Dwarf(),new Rogue(), new Inventory(10));
        PlayableCharacter mc = new PlayableCharacter("John", Gender.MALE, new Human(),new Warrior(), new Inventory(10));

        System.out.println(mc.whoAmI());
        System.out.println(mc.printAttributes());
        System.out.println(mc.getMaxHp());
        System.out.println(mc.getSp());

        System.out.println("---------------------");

        System.out.println(mc2.whoAmI());
        System.out.println(mc2.printAttributes());
        System.out.println(mc2.getMaxHp());
        System.out.println(mc2.getSp());






    }
}
