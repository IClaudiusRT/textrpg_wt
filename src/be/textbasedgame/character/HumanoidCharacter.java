package be.textbasedgame.character;

import be.textbasedgame.character.fantasyclass.FantasyClass;
import be.textbasedgame.character.race.Race;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.Combat;
import be.textbasedgame.combat.actions.attacks.WeaponAttack;
import be.textbasedgame.combat.actions.itemusage.HealthPotionUse;
import be.textbasedgame.items.HealthItem;
import be.textbasedgame.items.Inventory;
import be.textbasedgame.items.ItemVariables;
import be.textbasedgame.items.Weapon;
import be.textbasedgame.menus.InventoryMenu;
import be.textbasedgame.utility.ChoiceMenu;
import be.textbasedgame.utility.Die;
import be.textbasedgame.combat.Team;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public abstract class HumanoidCharacter extends Creature implements Serializable {
    private Gender gender;
    private Race race;
    private FantasyClass fc;
    private Inventory inventory;
    protected transient Map<String, ActionUse> actions;

    public HumanoidCharacter(String name, Gender gender, Race race, FantasyClass fc, Inventory inventory) {
        super(name);
        this.gender = gender;
        this.race = race;
        this.fc = fc;
        this.inventory = fc.initializeInventory();
        super.addAttributes(race.getAttributes());
        super.addAttributes(fc.getAttributes());
        this.actions = makeActionMap();
        super.setMaxHp(getMaxHp());
        super.setMaxSp(getMaxSp());
    }

    public Map<String, ActionUse> makeActionMap() {
        Map<String, ActionUse> act = new HashMap<>();
        act.put("attack", this::weaponAttack);
        act.put("skill", () -> fc.useSkill());
//        if (getInventory().makeConsumableMap().size() != 0) {
//            act.put("item use", this::itemUsage);
//        }
        return act;
    }

    public Action useAction() {
        return actions.get("attack").run();
    }

    public String getNameGreeting() {
        return "Your adventure name is: " + super.getName();
    }


    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public String getFc() {
        return fc.getDescription();
    }

    public void setFc(FantasyClass fc) {
        this.fc = fc;
        this.inventory=fc.initializeInventory();
    }

    public int getMaxHp() {
        return super.getAttributes().getConstitution() * 5;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public void getInventoryMenu() {
        InventoryMenu invMenu = new InventoryMenu(inventory);
        this.inventory = invMenu.inventoryMenu();

    }

    private int getMaxSp() {
        return fc.getSpecialPoint(super.getAttributes());
    }

    @Override
    public String getEncounterDescription() {
        return "a " + gender.toString().toLowerCase() + " " + race.toString() + " " + fc.toString();
    }

    @Override
    public void interact(Team initiator) {
        Combat combat = new Combat(initiator, new Team(this));
    }

    public String whoAmI() {
        return "Your name is " + super.getName() + ", you are a brave " + race.toString() + " and " + fc.toString() + ". ";

    }

    @Override
    public int rollForInitiative() {
        Die die = new Die(getAttributes().getDexterity());
        return die.roll();
    }

    public int getInvSize() {
        return race.getAttributes().getStrength() + fc.getAttributes().getStrength() + 10;
    }

    //TODO afhankelijk van wapen maken
    protected Action weaponAttack() {
        WeaponAttack attack = new WeaponAttack();
        Weapon defaultWeapon = new Weapon("fists", "ye ol' fisticuffs", ItemVariables.WEAPON, new Die[] {new Die(4)}, 0);
        Weapon weapon = getInventory().getEquipedWeapon();
        attack.setWeapon(Objects.requireNonNullElse(weapon, defaultWeapon));
        return attack;
    }

    protected Action itemUsage() {
        HealthPotionUse hpu = new HealthPotionUse();
        ChoiceMenu cm = new ChoiceMenu(getInventory().makeConsumableMap().keySet().toArray(new String[0]));
        hpu.setItem(getInventory().makeConsumableMap().get(cm.getChoice()));
        return hpu;
    }


    @Override
    public Creature target(Team targetOptions) {
        return targetOptions.random();
    }
}
