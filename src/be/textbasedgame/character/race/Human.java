package be.textbasedgame.character.race;

import be.textbasedgame.character.Attributes;

import java.io.Serializable;

public class Human implements Race, Serializable {

    private Attributes attributes = new Attributes(1,1,1,1,1,1);

    @Override
    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "Human";
    }
}
