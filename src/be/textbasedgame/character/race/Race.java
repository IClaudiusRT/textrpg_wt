package be.textbasedgame.character.race;

import be.textbasedgame.character.Attributes;

public interface Race {

    Attributes getAttributes();
}
