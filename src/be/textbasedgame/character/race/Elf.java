package be.textbasedgame.character.race;

import be.textbasedgame.character.Attributes;

import java.io.Serializable;

public class Elf implements Race, Serializable {

    private String name = "Elf";

    private Attributes attributes = new Attributes(0,3,0,3,0,0);

    @Override
    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "Elf";
    }

}
