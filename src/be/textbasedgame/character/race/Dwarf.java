package be.textbasedgame.character.race;

import be.textbasedgame.character.Attributes;

import java.io.Serializable;

public class Dwarf implements Race, Serializable {

    private Attributes attributes = new Attributes(3,0,0,0,3,0);



    @Override
    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "Dwarf";
    }
}
