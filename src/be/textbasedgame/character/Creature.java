package be.textbasedgame.character;

import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.Team;
import be.textbasedgame.map.Encounterable;

import java.io.Serializable;

public abstract class Creature implements Encounterable,Serializable {

    private String name;
    private int hp;
    private int maxHp;
    private int sp;
    private int maxSp;
    private Attributes attributes = new Attributes();

    public Creature(String name) {
        this.name = name;
    }

    public void addAttributes(Attributes atts){
        this.attributes.addAttributes(atts);

    }

    public Attributes getAttributes() {
        return attributes;
    }

    public abstract int rollForInitiative();
    /*
    New Character
    Base Attributes     -> Attributes.getAttributes
    Race Attributes     -> Human.getAttributes   (add)
    Class Attributes    -> Mage.getAttributes   (add)
    Final Attributes    ->  Total
     */


    public String printAttributes() {
        return attributes.printAttributes();
    }

    public int getHp() {
        return hp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
        this.hp = maxHp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean incapacitated() {
        return hp == 0;
    }

    public abstract Action useAction();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //placeholder for later
    public String getStatusEffects() {
        return "fine";
    }

    public void takeDamage(int damage) {
        this.hp = Math.max(this.hp - damage, 0);
    }

    @Override
    public String getPossibleAction() {
        return "attack " + name;
    }

    @Override
    public boolean finished() {
        return incapacitated();
    }

    public abstract Creature target(Team targetOptions);

    public int getSp() {
        return sp;
    }

    public void useSp(int sp) {
        this.sp = Math.max(this.sp - sp, 0);
    }

    protected void setMaxSp(int maxSp) {
        this.maxSp = maxSp;
        this.sp = maxSp;
    }

    public void heal(int heal) {
        this.hp = Math.min(hp+ heal, maxHp);
    }
}
