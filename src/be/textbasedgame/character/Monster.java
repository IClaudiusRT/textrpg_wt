package be.textbasedgame.character;

import be.textbasedgame.combat.Combat;
import be.textbasedgame.combat.Team;
import be.textbasedgame.combat.actions.Action;
import be.textbasedgame.combat.actions.attacks.MonsterAttack;
import be.textbasedgame.utility.Die;

public class Monster extends Creature {
    private int damageModifier;
    private Die[] damageDice;

    public Monster(String name, int hp, Die[] damageDice, int damageModifier) {
        super(name);
        super.setMaxHp(hp);
        this.damageDice = damageDice;
        this.damageModifier = damageModifier;
    }

    @Override
    public int rollForInitiative() {
        return new Die(10 + damageModifier).roll();
    }

    @Override
    public Action useAction() {
        return attack();
    }

    @Override
    public Creature target(Team targetOptions) {
        return targetOptions.random();
    }

    @Override
    public String getEncounterDescription() {
        return "you encounter a " + getName();
    }

    @Override
    public void interact(Team team) {
        Combat combat = new Combat(team, new Team(this));
        combat.start();
    }

    protected Action attack() {
        Action attack = new MonsterAttack(damageDice, damageModifier);
        return attack;
    }
}
