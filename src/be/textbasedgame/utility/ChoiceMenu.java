package be.textbasedgame.utility;

public class ChoiceMenu {

    private String[] choices;

//    public ChoiceMenu(String... choices) {
//        this.choices = choices;
//    }

    public ChoiceMenu(String[] choices) {
        this.choices= choices;
    }

    public String getChoice() {
        return ConsoleInput.getInstance().generalInputStrings(choices);
    }
}
