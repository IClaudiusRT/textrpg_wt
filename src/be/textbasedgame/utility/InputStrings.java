package be.textbasedgame.utility;

import java.util.ArrayList;
import java.util.List;

public enum InputStrings {

    CLASS_STRINGS(new String[]{"mage", "warrior", "rogue", "ranger"}),
    RACE_STRINGS(new String[]{"human", "elf", "dwarf", "dwarve", "elff"}),
    DIRECTION_STRINGS(new String[]{"north", "south", "east", "west"}),
    DIRECTION_LOOK_STRINGS(new String[]{"north", "south", "east", "west", "around"}),
    YESNO_STRINGS(new String[]{"yes", "no"}),
    MENUOPTIONS_STRINGS(new String[]{"new", "inventory", "load", "save", "reset", "controls", "continue", "quit"}),
    MENUOPTIONSINVENTORY_STRINGS(new String[]{"equip", "unequip", "quit", "consume", "hp", "drop"}),
    MENUOPTIONSINVENTORYINT_STRINGS(new String[]{"quit", "interact"}),
    GENDER_STRINGS(new String[]{"male", "female"}),
    MAPMENUOPTIONS_STRINGS(new String[]{"look", "go"});

    String[] inputStrings;

    InputStrings(String[] inputStrings) {
        this.inputStrings = inputStrings;

    }

    public String[] getInputStrings() {
        return inputStrings;
    }
}
