package be.textbasedgame.utility;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Predicate;

public class ConsoleInput {
    private static ConsoleInput consoleInput_instance = null;

    private ConsoleInput() {

    }

    public static ConsoleInput getInstance() {
        if (consoleInput_instance == null) {
            consoleInput_instance = new ConsoleInput();
        }
        return consoleInput_instance;
    }

    public int getInt(int max) {
        boolean correctInput = false;
        int number = 0;
        do {
            Scanner keyboard = new Scanner(System.in);
            try {
                number = keyboard.nextInt();
            } catch (Exception e) {
                System.out.println("Input is not a valid number");
            }
            if (number >= 1 && number <= max) {
                correctInput = true;
            } else {
                System.out.println("Input is not between 1 and " + max);
            }
        } while (!correctInput);
        return number;
    }

    public String generalInputStrings(String[] strings) {
        System.out.println("Valid choices: " + Arrays.toString(strings));
        String [] lowerCaseOptions = Arrays.stream(strings).map(String::toLowerCase).toArray(String[]::new);
        boolean correctInput = false;
        String input =" ";
        do {
            Scanner keyboard = new Scanner(System.in);
            try {
                input= keyboard.nextLine().toLowerCase();
            } catch (Exception e) {
                System.out.println("Input is not valid");
            }
            String finalInput = input;
            if (Arrays.asList(lowerCaseOptions).contains(finalInput)){
                correctInput= true;
            } else {
                System.out.println("Input is not a valid keyword.");
            }
        } while (!correctInput);
        return input;
    }

    public String generalInputStrings(InputStrings inputStrings){
        return generalInputStrings(inputStrings.getInputStrings());
    }

    public void waitForInput() {
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String nextString() {
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextLine();
    }

    public boolean confirmChoice() {
        System.out.println("Are you happy with your choice?");
        System.out.println("Yes or No?");
        String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.YESNO_STRINGS);
        if (choice.equals("yes")) {
            return false;
        } else {
            return true;
        }
    }


}
