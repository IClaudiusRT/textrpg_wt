package be.textbasedgame.utility;

import java.io.Serializable;
import java.util.Random;

public class Die implements Serializable {
    private Random random;
    private int max;

    public Die(int max) {
        this.random = new Random();
        this.max = max;
    }

    public int roll() {
        return random.nextInt(max);
    }
}
