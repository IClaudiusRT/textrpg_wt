package be.textbasedgame.utility;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.Team;
import jdk.swing.interop.SwingInterOpUtils;

import java.sql.SQLOutput;
import java.util.Arrays;

public class Printer {

    private static Printer printer_instance = null;

    private Printer() {

    }

    public static Printer getInstance() {
        if (printer_instance == null) {
            printer_instance = new Printer();
        }
        return printer_instance;
    }

    public void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public void listTeam(Team team) {
        int index = 1;
        for (Creature c : team.getTeamAsArray()) {
            System.out.println(index + ". " + c.getName() + " (" + c.getHp() + " HP)");
        }
    }

    public void teamCondition(Team team, String name) {
        System.out.println();
        System.out.println(name);
        for (Creature c : team.getTeamAsArray()) {
            System.out.println(c.getName() + " " + c.getHp() + " HP and is " + c.getStatusEffects());
        }
    }
    

    public void initiativeRoll(Creature c, int initiative) {
        System.out.println(c.getName() + " rolled a " + initiative);
    }

    public void damageMessage(Creature c, Creature target, int damage) {
        clearConsole();
        System.out.println(c.getName() + " dealt " + damage + " to " + target.getName() );
    }

    public void turnMessage(Creature c) {
        System.out.println(c.getName() + " gets a turn");
    }

    public void creatureCondition(Creature c) {
        System.out.println();
        System.out.println(c.getName() + " is currently " + c.getStatusEffects());
    }

    public void targetQuestion() {
        clearConsole();
        System.out.println("Who do you want to target?");
    }

    public void listOrder(Creature[] order) {
    }

    public void listPossibleActions(String[] possibleActions) {
        System.out.println();
        int index = 1;
        for (String a : possibleActions) {
            System.out.println(index++ + ". " + a);
        }
    }

    public void inBetween(){
        System.out.println("----------------------");
    }

    public void gameOverMessage() {
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("------------------------");
        System.out.println("/                      /");
        System.out.println("/      GAME  OVER!     /");
        System.out.println("/                      /");
        System.out.println("------------------------");
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public void savedSuccessfully() {
        System.out.println("Game saved!");
    }

    public void damageMessage(String attacker, int damage, String attackName, String target) {
        System.out.println(attacker + " did " + damage + " damage with " + attackName + " to " + target);
    }

    public void actionNotPossible() {
        System.out.println("This action is not possible");
    }
}
