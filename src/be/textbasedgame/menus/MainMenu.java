package be.textbasedgame.menus;

import be.textbasedgame.Game;
import be.textbasedgame.character.*;
import be.textbasedgame.character.fantasyclass.*;
import be.textbasedgame.character.race.*;
import be.textbasedgame.items.Inventory;
import be.textbasedgame.map.Map;
import be.textbasedgame.map.Maps;
import be.textbasedgame.utility.ConsoleInput;
import be.textbasedgame.utility.InputStrings;
import be.textbasedgame.utility.Printer;

import java.io.*;
import java.lang.ref.ReferenceQueue;

public class MainMenu {

    private static MainMenu mmInstance;
    boolean programRunning = true;
    Maps testMap = new Maps();
    Map mainMap = testMap.loadMapTwo();
    PlayableCharacter mainPC; // = new PlayableCharacter(" ", Gender.MALE, new Human(), new Rogue(), new Inventory(10));
    Game mainGame; // = new Game(testMap.loadMapTwo(mainMap), mainPC);

    public static MainMenu getInstance() {
        if (mmInstance == null) {
            mmInstance = new MainMenu();
        }
        return mmInstance;
    }

    public MainMenu() {
        System.out.println("Booting up...");
        Printer.getInstance().inBetween();
        //saveGame("LegendofTheLamb_autosave");
    }

    public void continueProgram() {
        while (programRunning) {
            mainMenu();
        }
    }

    void closeProgram() {
        programRunning = false;
        System.out.println("Until next time.");
        System.exit(0);
    }


    public void mainMenu() {
        Printer.getInstance().inBetween();
        System.out.println("Welcome to The Legend of the Lamb III\nPlease choose what you would like to do.");
        Printer.getInstance().inBetween();
        System.out.println(" New - Start a New Game");
        System.out.println(" Load - Load a Saved Game ");
        System.out.println(" Save - Save the Game");
        System.out.println(" Reset - Reset all Saved Files");
        System.out.println(" Controls - Game Controls");
        System.out.println(" Continue - Continue the Game");
        System.out.println(" Quit - Quit Game");
        Printer.getInstance().inBetween();
        String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.MENUOPTIONS_STRINGS);
        switch (choice) {
            case "new":
                createCharacter();
                break;
            case "load":
                mainGame = loadGame("LegendofTheLamb_autosave");
                mainGame.start();
                break;
            case "save":
                saveGame("LegendofTheLamb_autosave");
                break;
            case "reset":
                resetAllSaves();
                break;
            case "controls":
                getControls();
                break;
            case "continue":
                continueGame();
                break;
            case "quit":
                closeProgram();
                break;
        }
    }

    private void continueGame() {
        loadGame("LegendofTheLamb_autosave").start();
    }

    public void createCharacter() {
        mainPC = new PlayableCharacter(
                characterCreateNewName(),
                characterCreateGender(),
                characterCreateRace(),
                characterCreateClass(),
                new Inventory(10));
        mainGame = new Game(mainMap, mainPC);
        characterPrintSynopsis();
        mainGame.start();
    }

    public String characterCreateNewName() {
        String name;
        do {
            System.out.println("What is your character's name?");
            name = ConsoleInput.getInstance().nextString();
            Printer.getInstance().inBetween();
            System.out.println("You chose the name: " + name + ".\n It's a fine name.");
            //mainPC.setName(name);
            Printer.getInstance().inBetween();

        } while (ConsoleInput.getInstance().confirmChoice());
        return name;
    }

    public Gender characterCreateGender() {
        Gender gender;
        do {
            System.out.println("What is the gender of your character?");
            System.out.println("Male or Female?");
            String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.GENDER_STRINGS);
            if (choice.equals("male")) {
                System.out.println("Very well.");
                Printer.getInstance().inBetween();
                gender = Gender.MALE;
            } else {
                System.out.println("Very well.");
                gender = Gender.FEMALE;
            }
        } while (ConsoleInput.getInstance().confirmChoice());
        return gender;
    }

    public Race characterCreateRace() {
        Race race = new Human();
        do {
            System.out.println("What race would you like your character to be?");
            System.out.println("Dwarf, Human or Elf?");
            String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.RACE_STRINGS);
            switch (choice) {
                case "dwarf":
                    System.out.println("A stout Dwarf, good choice.");
                    Printer.getInstance().inBetween();
                    race = new Dwarf();
                    break;
                case "human":
                    System.out.println("A Human, great in all situations, good choice.");
                    Printer.getInstance().inBetween();
                    race = new Human();
                    break;
                case "elf":
                    System.out.println("An Elf, wise and powerful, good choice.");
                    Printer.getInstance().inBetween();
                    race = new Elf();
                    break;
            }
        } while (ConsoleInput.getInstance().confirmChoice());
        return race;
    }

    public FantasyClass characterCreateClass() {
        FantasyClass fc = new Warrior();
        do {
            System.out.println("We are almost there. What class should your character be?");
            System.out.println("Warrior, Rogue, Ranger or Mage?");
            String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.CLASS_STRINGS);
            switch (choice) {
                case "warrior":
                    System.out.println("You chose a Warrior.");
                    Printer.getInstance().inBetween();
                    fc = new Warrior();
                    break;
                case "rogue":
                    System.out.println("You chose a Rogue.");
                    Printer.getInstance().inBetween();
                    fc = new Rogue();
                    break;
                case "ranger":
                    System.out.println("You chose a Ranger");
                    Printer.getInstance().inBetween();
                    fc = new Ranger();
                    break;
                case "mage":
                    System.out.println("You chose a Mage.");
                    Printer.getInstance().inBetween();
                    fc = new Mage();
                    break;
            } //mainPC.getInventory().printItems();
        } while (ConsoleInput.getInstance().confirmChoice());
        return fc;
    }

    public void characterPrintSynopsis() {
        System.out.println(mainPC.whoAmI());
    }

    public PlayableCharacter getMainPC(){
        return mainPC;
    }

    public void saveGame(String saveName) {
        mainGame.save();
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(saveName + ".txt"))) {
            out.writeObject(mainGame);
            Printer.getInstance().savedSuccessfully();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Game loadGame(String saveName) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(saveName + ".txt"))) {
            System.out.println("Game has loaded successfully!");
            return (Game) in.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void resetAllSaves(){
        File fileToBeDeleted = new File("LegendofTheLamb_autosave.txt");
        fileToBeDeleted.delete();
        System.out.println("You have successfully deleted your save files.");
    }


    public void getControls(){
        System.out.println("\tWelcome to The Legend of the Lamb III! \nThis game uses written commands, for example if you want to equip an item simply type the commands \"item\" and \"equip\". \nDont worry with every choice the possible commands you can type will be displayed first.\nWe hope you have fun!");
    }
}
