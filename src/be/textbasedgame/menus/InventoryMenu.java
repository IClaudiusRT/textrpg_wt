package be.textbasedgame.menus;

import be.textbasedgame.items.Inventory;
import be.textbasedgame.items.Item;
import be.textbasedgame.utility.ConsoleInput;
import be.textbasedgame.utility.InputStrings;

public class InventoryMenu {

    boolean programRunning = true;
    Inventory tempInventory;
    int countIndex = 0;

    public InventoryMenu(Inventory inventory) {
        tempInventory = inventory;

    }

    public void continueProgram() {
        while (programRunning) {
            inventoryMenu();
        }
    }

    void closeProgram() {
        programRunning = false;
    }


    public Inventory inventoryMenu() {
        while (programRunning) {
            tempInventory.printItems();
            System.out.println("Would you like to interact with an item in your inventory?\nType interact or quit.");
            String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.MENUOPTIONSINVENTORYINT_STRINGS);
            switch (choice) {
                case "interact":
                    selectItem();
                    break;
                case "quit":
                    closeProgram();
                    break;
            }
        }
        return tempInventory;

    }

    public void selectItem() {
        String names = " ";
        tempInventory.printItems();
        System.out.println("Which item would you like to interact with?");
        String choice = ConsoleInput.getInstance().generalInputStrings(tempInventory.getListNames());
        for (Item el : tempInventory.listInventory(tempInventory)) {
            names = el.getName().toLowerCase();
            if (choice.equals(names)) {
                printOptions(el);
                break;
            }
        }
    }

    public void printOptions(Item chosenItem) {
        System.out.println(chosenItem.getDescription());
        if (chosenItem.getEnumType().isEquippable()) {
            System.out.println("Equip - Equip this item");
        }
        if (chosenItem.getEnumType().isEquipped(chosenItem)) {
            System.out.println("Unequip - Unequip the selected item");
        }
        if (chosenItem.getEnumType().isConsumable()) {
            System.out.println("Consume - Consume the item");
        }
        if (chosenItem.getEnumType().isDroppable()) {                 //if you drop an equipped item, reset equipped item TODO
            System.out.println("Drop - Drop this item on ground");
        }
        String choice = ConsoleInput.getInstance().generalInputStrings(InputStrings.MENUOPTIONSINVENTORY_STRINGS);
        switch (choice) {
            case "equip":
                tempInventory.equipItem(chosenItem);
                System.out.println("The item has been equipped!");
                break;
            case "unequip":
                tempInventory.removeEquippedItem(chosenItem);
                System.out.println("The item has been succesfully unequipped!");
                break;
            case "consume":
                tempInventory.consumeItem(chosenItem);
                break;
            case "drop":
                tempInventory.removeItem(chosenItem);
                System.out.println("The item has been removed!");
                break;
        }
    }


}
