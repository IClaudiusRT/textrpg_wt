package be.textbasedgame;

import be.textbasedgame.character.PlayableCharacter;
import be.textbasedgame.map.Map;
import be.textbasedgame.menus.MainMenu;
import be.textbasedgame.utility.Printer;

import java.io.*;

public class Game implements Serializable {
    private PlayableCharacter mainCharacter;
    private Map map;

    public Game(Map map, PlayableCharacter pc) {
        this.map = map;
        this.mainCharacter = pc; //vervangen door team?
        }

    public void setMap(Map map) {
        this.map = map;
    }

    public void start() {
        map.setHeroes(mainCharacter);
        System.out.println(map.getTileDescription());
        while (gameGoing()) {
            nextAction();
        }
        Printer.getInstance().gameOverMessage();
    }

    private boolean gameGoing() {
        return mainCharacter.getHp() != 0;
    }

    private void nextAction() {
        map.chooseAction();
    }

    public PlayableCharacter getMainPC() {
        return mainCharacter;
    }

    public void save() {
        map.saveSpawn();
    }
}
