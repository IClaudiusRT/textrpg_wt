package be.textbasedgame.map;

import be.textbasedgame.character.Creature;
import be.textbasedgame.combat.Team;

public interface Encounterable {
    String getEncounterDescription();

    void interact(Team team);

    String getPossibleAction();

    boolean finished();
}
