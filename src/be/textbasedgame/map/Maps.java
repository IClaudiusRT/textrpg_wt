package be.textbasedgame.map;

import be.textbasedgame.character.Gender;
import be.textbasedgame.character.Monster;
import be.textbasedgame.character.NonPlayableCharacter;
import be.textbasedgame.character.fantasyclass.Mage;
import be.textbasedgame.character.race.Elf;
import be.textbasedgame.character.PlayableCharacter;
import be.textbasedgame.character.fantasyclass.*;
import be.textbasedgame.character.race.*;

import be.textbasedgame.items.Inventory;
import be.textbasedgame.utility.Die;

public class Maps {

    public Map loadMapOne(Map map) {
        map = new Map();
        map.newEmptyMap(3, 3);

        Tile forestPath = new Tile(true, "a forest with a path", "a forest");
        Tile openForest = new Tile(true, "a open forest","an open forest");
        Tile lake = new Tile(false, "a shallow lake","a shallow lake");
        Tile denseForest = new Tile(false, "a dense impassable forest","a dense forest");

        map.setTile(forestPath, 0, 0);
        map.setTile(openForest, 1, 0);
        map.setTile(lake, 2, 0);

        map.setTile(forestPath, 0, 1);
        map.setTile(forestPath, 1, 1);
        map.setTile(forestPath, 2, 1);

        map.setTile(denseForest, 0, 2);
        map.setTile(openForest, 1, 2);
        map.setTile(forestPath, 2, 2);

        NonPlayableCharacter hostile_elf = new NonPlayableCharacter("a hostile elf", Gender.FEMALE, new Elf(), new Mage(), new Inventory(10));
        openForest.setEncounterables(hostile_elf);

        return map;
    }

    public Map loadMapTwo() {
        Map map = new Map();
        map.newEmptyMap(5, 5);
        Monster monsterOne = new Monster("wolf", 20, new Die[]{new Die(4)}, 1);
        Monster monsterTwo = new Monster("goblin", 20, new Die[]{new Die(4)}, 3);
        Monster monsterThree = new Monster("angry Tree", 35, new Die[]{new Die(6)}, 2);


        Tile tileZeroZero = new Tile(true, "You find yourself on a simple road surrounded by shrubbery and some sparse trees.","a simple road surrounded by some shrubbery and trees.");
        map.setTile(tileZeroZero, 0, 0);
        Tile tileZeroOne = new Tile(true, "You find yourself on a dirt road. The road is slowly being taken over by mother nature this makes it hard to determine exactly where it leads.","a simple road surrounded by some shrubbery and trees.");
        map.setTile(tileZeroOne, 0, 1);
        Tile tileZeroTwo = new Tile(true, "You are on a dirt road that is barely noticeable, the surrounding shrubbery increases with every step you take sometimes hampering your movement.","a barely visible dirt road surrounded by an almost forest.");
        map.setTile(tileZeroTwo, 0, 2);
        Tile tileZeroThree = new Tile(false, " ","a river that streams through the land, with dangerous torrents, it doesn't seem you can pass it without at least a raft.");
        map.setTile(tileZeroThree, 0, 3);

        Tile tileOneZero = new Tile(false, " ","an impregnable forest in your way, this way doesn't seem like a viable option.");
        map.setTile(tileOneZero, 1, 0);
        Tile tileOneOne = new Tile(false, " ","a dark forest that continues, when you try to peer inside the only thing you spot are thorns sharp as knives weaving their way throughout the trees");
        map.setTile(tileOneOne, 1, 1);
        Tile tileOneTwo = new Tile(true, "You find yourself on a dirt road that meanders further into the land, at times barely noticeable, but with your pathfinding skills you are able to stay true to your course.","a barely noticeable dirt path weaving through the land.", monsterOne);
        map.setTile(tileOneTwo, 1, 2);
        Tile tileOneThree = new Tile(false, " ","a river that rages forth, blocking your path.");
        map.setTile(tileOneThree, 1, 3);
        Tile tileOneFour = new Tile(false, " ", "a steep impassible ravine.");
        map.setTile(tileOneFour, 1, 4);

        Tile tileTwoZero = new Tile(false, " ","a Dark forest that looms ahead, blocking your movement.");
        map.setTile(tileTwoZero, 2, 0);
        Tile tileTwoOne = new Tile(true, "You are on a dirt road that has almost evolved into pure wilderness, but there is still a visible clearing you can follow."," a clearing through the forest that resembles a path, or at least a way forward.");
        map.setTile(tileTwoOne, 2, 1);
        Tile tileTwoTwo = new Tile(true, "You find yourself on a dirt road that has almost faded, overtaken by mother nature, you are surrounded by trees and hear forest animals all around you.","an almost faded dirt road surrounded by what looks to be light forest.");
        map.setTile(tileTwoTwo, 2, 2);
        Tile tileTwoThree = new Tile(false, " ","the river that starts to bend southwards, still you would be hardpressed to cross it, another path should be more prudent.");
        map.setTile(tileTwoThree, 2, 3);
        Tile tileTwoFour = new Tile(true, "You enter a field with on it the remains of some kind of structure. You can see massive stone rubble everywhere. Ancient but clearly man-made.", "an open field with in the middle some kind of ruins.",monsterThree);
        map.setTile(tileTwoFour, 2, 4);

        Tile tileThreeZero = new Tile(true, "You find yourself between wild shrubbery,hiding under it you seem to notice another path, if it even is a path, but you think you could traverse it if pressed.","a lot of shrubbery, and oh? What's that?");
        map.setTile(tileThreeZero, 3, 0);
        Tile tileThreeOne = new Tile(true, "You traverse a clearing that runs through this almost jungle, forest animals can be heard all around you, sometimes even these sound sinister","a clearing cutting through what almost seems like jungle.");
        map.setTile(tileThreeOne, 3, 1);
        Tile tileThreeTwo = new Tile(false, " ", "the raging river that seems to end here, being pulled into the earth to continue it's destructive torrents out of sight for a while. The river end is surrounded by sharp rocks wittled down by centuries of flowing water.");
        map.setTile(tileThreeTwo, 3, 2);
        Tile tileThreeThree = new Tile(false, " ", "a river that sets it's course to the south and seems to be increasing in speed.");
        map.setTile(tileThreeThree, 3, 3);
        Tile tileThreeFour = new Tile(true, "You continue your way through a forestry area with an ancient stone roads that seems to come to an end, scattering itself over its surroundings","an ancient stone road.");
        map.setTile(tileThreeFour, 3, 4);

        Tile tileFourZero = new Tile(false, " ", "a place covered in jutting razorsharp rocks.");
        map.setTile(tileFourZero, 4, 0);
        Tile tileFourOne = new Tile(true, "You enter an forestlike environment with a new road that starts to emerge, different than the dirt road form before, this seems to have been paved stones but has so far deteriorated that not much of that remains.","a sort of path cutting through forest. You can't make out many details from her.");
        map.setTile(tileFourOne, 4, 1);
        Tile tileFourTwo = new Tile(true, "You find yourself still in a small forest with a stonepaved road that becomes a bit clearer beneath your feet, surrounded by a small, but certainly welcomed clearing.","a small clearing with a stone path snaking through.", monsterTwo);
        map.setTile(tileFourTwo, 4, 2);
        Tile tileFourThree = new Tile(true, " Continuing forward through a forest, not much of a change in scenery, the remains of the stone road continue northwards.","still more forest, the paves stone road continuing ever forwards.");
        map.setTile(tileFourThree, 4, 3);
        Tile tileFourFour = new Tile(true, "You come upon a field, the previous light forest that has surrounded you has been passed. The stone road you're travelling suddenly turns to the west; you hope it doesn't just circle back to your starting point.","a field, the stone road you have been following seems to bent to thet west.");
        map.setTile(tileFourFour, 4, 4);

        return map;
    }
}
