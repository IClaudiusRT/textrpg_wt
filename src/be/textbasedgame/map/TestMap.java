package be.textbasedgame.map;

import be.textbasedgame.Game;
import be.textbasedgame.character.fantasyclass.Mage;
import be.textbasedgame.character.fantasyclass.Rogue;
import be.textbasedgame.character.fantasyclass.Warrior;
import be.textbasedgame.character.race.*;
import be.textbasedgame.items.Inventory;
import be.textbasedgame.items.Loot;
import be.textbasedgame.character.*;
import be.textbasedgame.utility.Die;

public class TestMap {
    public static void main(String[] args) {

        //testMapWithSimpleCombat();

        test3by3Map();

    }

    private static void test3by3Map() {
        PlayableCharacter pc = new PlayableCharacter("John", Gender.MALE, new Dwarf(), new Warrior(), new Inventory(10));
        NonPlayableCharacter hostile_elf = new NonPlayableCharacter("elf", Gender.FEMALE, new Elf(), new Mage(), new Inventory(10));
        Monster wolf = new Monster("wolf", 20, new Die[] {new Die(6)}, 0);

        Map map = new Map();
        map.newEmptyMap(3,3);

        Tile forestPath = new Tile(true, "a forest with a path","a forest with a path");
        Tile openForest = new Tile(true, "a open forest","a open forest");
        Tile lake = new Tile(false, "a shallow lake","a shallow lake");
        Tile denseForest = new Tile(false, "a dense impassable forest","a dense impassable forest");

        map.setTile(forestPath, 0, 0);
        map.setTile(new Tile(openForest, hostile_elf), 1, 0);
        map.setTile(lake, 2, 0);

        map.setTile(new Tile(forestPath, wolf),0, 1);
        map.setTile(forestPath,1, 1);
        map.setTile(forestPath,2, 1);

        map.setTile(denseForest, 0,2);
        map.setTile(openForest, 1, 2);
        map.setTile(new Tile(forestPath, hostile_elf), 2, 2);

        Game game = new Game(map, pc);
        game.start();

    }

    private static void testMapWithSimpleCombat() {

        Map map = new Map();

        PlayableCharacter pc = new PlayableCharacter("John", Gender.MALE, new Elf(), new Mage(), new Inventory(10));
        NonPlayableCharacter monster = new NonPlayableCharacter("Angry Dwarf", Gender.FEMALE, new Dwarf(), new Rogue(), new Inventory(10));

        map.newEmptyMap(1, 3);

        map.setTile(new Tile(true, "a forest with a path"," "), 0, 0);
        map.setTile(new Tile(true, "a forest with a path"," "), 0, 1);
        map.setTile(
                new Tile(true,
                        "a forest with a path"," ",
                        monster,
                        new Loot("Chest",null))
                , 0, 2);

        map.setPos(0,1);

        map.look("north");
        map.go("north");
        map.interact(0);
    }
}
