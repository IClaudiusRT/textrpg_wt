package be.textbasedgame.map;

import be.textbasedgame.character.HumanoidCharacter;
import be.textbasedgame.character.PlayableCharacter;
import be.textbasedgame.combat.Team;
import be.textbasedgame.menus.MainMenu;
import be.textbasedgame.utility.ChoiceMenu;
import be.textbasedgame.utility.ConsoleInput;
import be.textbasedgame.utility.InputStrings;

import java.io.Serializable;
import java.util.HashMap;

public class Map implements Serializable {
    private Tile[][] tiles;
    private Team heroes;
    private int spawnX;
    private int spawnY;
    private int x = spawnX;
    private int y = spawnY;

    public void setTile(Tile tile, int x, int y) {
        if (inMap(x, y)) {
            tiles[x][y] = tile;
        } else {
            System.out.println("Tile outside map boundaries");
        }
    }

    public int[] getPosition() {
        return new int[] {x, y};
    }

    private Tile currentTile() {                    //TODO hier ligt het probleem met het loaden
        return tiles[x][y];
    }

    public void loadTiles(Tile[][] tiles) {
        this.tiles = tiles;
    }

    public void newEmptyMap(int maxX, int maxY) {
        this.tiles = new Tile[maxX][maxY];
    }

    public void setPos(int x, int y) {
        if (inMap(x, y) && tiles[x][y].isPassable()) {
            this.x = x;
            this.y = y;
            System.out.println(currentTile().getEncounterDescription());
            //tiles[x][y].possibleActions();
        } else {
            System.out.println("Impossible movement");
        }
    }

    public void setHeroes(PlayableCharacter... heroes) {
        this.heroes = new Team(heroes);
    }

    public void setSpawn(int x, int y) {
        spawnX = x;
        spawnY = y;
    }

    public void look(String direction) {
        StringBuilder sb = new StringBuilder();
        switch (direction) {
            case "north":
                sb.append("To the north lies ").append(getTileDescriptionFar(x, y + 1));
                break;
            case "east":
                sb.append("To the east lies ").append(getTileDescriptionFar(x + 1, y));
                break;
            case "south":
                sb.append("To the south lies ").append(getTileDescriptionFar(x, y - 1));
                break;
            case "west":
                sb.append("To the west lies ").append(getTileDescriptionFar(x - 1, y));
                break;
            case "around":
                sb.append("To the north lies ").append(getTileDescriptionFar(x, y + 1));
                sb.append("\nTo the east lies ").append(getTileDescriptionFar(x + 1, y));
                sb.append("\nTo the south lies ").append(getTileDescriptionFar(x, y - 1));
                sb.append("\nTo the west lies ").append(getTileDescriptionFar(x - 1, y));
                break;
        }
        System.out.println(sb.toString());
    }

    private boolean inMap(int x, int y) {
        return (x <= tiles.length - 1 && x >= 0 && y <= tiles[x].length - 1 && y >= 0);
    }

    private String getTileDescription(int x, int y) {
        if (inMap(x, y)) {
            return tiles[x][y].getEncounterDescription();
        } else {
            return "Impassible terrain";
        }
    }

    private String getTileDescriptionFar(int x, int y) {
        if (inMap(x, y)) {
            return tiles[x][y].getEncounterDescriptionFar();
        } else {
            return "Impassible terrain";
        }
    }


    public String getTileDescription() {
        return currentTile().getEncounterDescription();
    }

    public void go(String direction) {
        switch (direction) {
            case "north":
                setPos(x, y + 1);
                break;
            case "east":
                setPos(x + 1, y);
                break;
            case "south":
                setPos(x, y - 1);
                break;
            case "west":
                setPos(x - 1, y);
                break;
        }
    }

    public void interact(int index) {
        currentTile().interact(index, heroes);
    }

//    public void chooseAction() {
//        int actions = tiles[x][y].possibleActions();
//        int choice = ConsoleInput.getInstance().getInt(actions);
//        if (choice == 1) {
//            String direction = ConsoleInput.getInstance().generalInputStrings(InputStrings.DIRECTION_STRINGS);
//            look(direction);
//        } else if (choice == 2) {
//            String direction = ConsoleInput.getInstance().generalInputStrings(InputStrings.DIRECTION_STRINGS);
//            go(direction);
//        } else {
//            tiles[x][y].interact(choice - 3, heroes);
//        }
//    }

    public java.util.Map<String, TeamRunnable> makeActionMap() {
        java.util.Map<String, TeamRunnable> actionmap = new HashMap<>();
        //Standard methods
        actionmap.put("look", team -> look(ConsoleInput.getInstance().generalInputStrings(InputStrings.DIRECTION_LOOK_STRINGS)));
        actionmap.put("go", team -> go(ConsoleInput.getInstance().generalInputStrings(InputStrings.DIRECTION_STRINGS)));
        actionmap.put("inventory", team -> ((HumanoidCharacter) team.random()).getInventoryMenu());
        actionmap.put("menu", team -> MainMenu.getInstance().mainMenu());
        //Tile methods
        for (String s : currentTile().makeRunnableMap().keySet().toArray(new String[0])) {
            actionmap.put(s, team -> currentTile().interact(s, team));
        }

        return actionmap;
    }

    public void chooseAction() {
        java.util.Map<String, TeamRunnable> actionMap = makeActionMap();
        ChoiceMenu cm = new ChoiceMenu(actionMap.keySet().toArray(new String[0]));
        actionMap.get(cm.getChoice()).run(heroes);
    }

    public void saveSpawn() {
        setSpawn(x, y);
    }
}

