package be.textbasedgame.map;

import be.textbasedgame.combat.Team;

public interface TeamRunnable {

    void run(Team team);
}
