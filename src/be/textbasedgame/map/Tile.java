package be.textbasedgame.map;

import be.textbasedgame.combat.Team;
import be.textbasedgame.utility.Printer;

import java.io.Serializable;
import java.util.*;

public class Tile implements Serializable {
    private boolean passable;
    private List<Encounterable> encounterables = new ArrayList<>();
    private String description;
    private String farDescription;

    public Tile(boolean passable, String description, String farDescription, Encounterable... encounterables) {
        this.passable = passable;
        this.description = description;
        this.farDescription = farDescription;
        setEncounterables(encounterables);
    }

    public Tile(Tile tile, Encounterable... encounterables) {
        this(tile.isPassable(), tile.getDescription(), tile.getFarDescription(), tile.getEncounterables().toArray(new Encounterable[0]));
        addEncounterables(encounterables);
    }

    private List<Encounterable> getEncounterables() {
        return encounterables;
    }

    public Tile(boolean passable) {
        this.passable = passable;
    }

    public String getDescription() {
        return description;
    }

    public String getFarDescription() {
        return farDescription;
    }

    public boolean isPassable() {
        return passable;
    }

    public void setEncounterables(Encounterable... encounterables) {
        this.encounterables = new LinkedList<>(Arrays.asList(encounterables));
    }

    public void addEncounterables(Encounterable... encounterables) {
        Collections.addAll(this.encounterables, encounterables);
    }

    public String getEncounterDescription() {
        StringBuilder sb = new StringBuilder(description);
        for (Encounterable e : encounterables) {
            sb.append(", ").append(e.getEncounterDescription());
        }
        return sb.toString();
    }

    public String getEncounterDescriptionFar() {
        StringBuilder sb = new StringBuilder(farDescription);
        return sb.toString();
    }

    public void interact(int index, Team initiator) {
        Encounterable e = encounterables.get(index);
        e.interact(initiator);
        if (e.finished()) {
            encounterables.remove(index); //TODO make new encounterable bv. after fight you can loot the corpse
        }
    }

    public void interact(String keyword, Team team) {
        makeRunnableMap().get(keyword).run(team);
    }

    //doesn't work
    private void removeEncounterable(Encounterable encounterable) {
        this.encounterables.remove(encounterable);
    }

    public int possibleActions() {
        ArrayList<String> actions = new ArrayList<>();
        actions.add("look");
        actions.add("go");
        for (Encounterable e : encounterables) {
            actions.add(e.getPossibleAction());
        }
        String[] act = actions.toArray(new String[actions.size()]);
        Printer.getInstance().listPossibleActions(act);
        return actions.size();
    }

    public java.util.Map<String, TeamRunnable> makeRunnableMap() {
        java.util.Map<String, TeamRunnable> runnableMap = new HashMap<>();
        for (Encounterable e : encounterables) {
            runnableMap.put(e.getPossibleAction(), e::interact);
        }
        return runnableMap;
    }

}
